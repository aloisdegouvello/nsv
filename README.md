# Newline-separated values (NSV)

<p style="text-align: center">
<img style="height:128px;width:128px;" src="./asset/nsv.png" alt="logo"/>
</p>

**Newline-separated values** (NSV) is a text file format that uses [newlines] to separate values. A NSV file stores tabular data (numbers and text) in plain text, where each line of the file typically represents one data record. Each record consists of the same number of fields, and these are separated by commas in the CSV file. If the field delimiter itself may appear within a field, fields can be surrounded with quotation marks. NSV is inspired by the [SubRip Text file format][srt].

The NSV file format is one type of [delimiter-separated file format][dsv] like csv or tsv. NSV files are given a ".nsv" extension. NSV's media type is `text/nsv`.

The goal of NSV is not to be strictly better than CSV or any other DSV. The aim is to offer an alternative to human like me who prefer to read this format than CSV or any other DSV. In a lot of case, CSV or TSV work perfectly fine for most people including myself. We trade horizontal space for vertical space.

[newlines]: https://en.wikipedia.org/wiki/Newline
[dsv]: https://en.wikipedia.org/wiki/Delimiter-separated_values
[csv]: https://en.wikipedia.org/wiki/Comma-separated_values
[tsv]: https://en.wikipedia.org/wiki/Tab-separated_values
[srt]: https://en.wikipedia.org/wiki/SubRip

## Examples

### NSV and CSV

<!-- https://stackoverflow.com/q/11700487/1248177 -->

| Year |  Make |                  Model                 |            Description            |  Price   |
|-----:|:------|:---------------------------------------|:----------------------------------|--------:|
| 1997 | Ford  | E350                                   | ac, abs, moon                     | 3000.00  |
| 1999 | Chevy | Venture "Extended Edition"             |                                   | 4900.00  |
| 1999 | Chevy | Venture "Extended Edition, Very Large" |                                   | 5000.00  |
| 1996 | Jeep  | Grand Cherokee                         | MUST SELL!<br>air, moon roof, loaded | 4799.00  |


The above table of data may be represented in NSV format as follows: 

```nsv
Year
Make
Model
Description
Price

1997
Ford
E350
ac, abs, moon
3000.00

1999
Chevy
Venture "Extended Edition"
""
4900.00

1999
Chevy
Venture "Extended Edition, Very Large"
""
5000.00

1996
Jeep
Grand Cherokee
"MUST SELL!
air, moon roof, loaded"
4799.00
```

and in CSV as

```csv
Year,Make,Model,Description,Price
1997,Ford,E350,"ac, abs, moon",3000.00
1999,Chevy,"Venture ""Extended Edition""","",4900.00
1999,Chevy,"Venture ""Extended Edition, Very Large""","",5000.00
1996,Jeep,Grand Cherokee,"MUST SELL!
air, moon roof, loaded",4799.00
```

In the case above CSV is doing quite ok let's show another example

```csv
date,titre,lien
6 février,Journée internationale de la tolérance zéro à l'égard des mutilations génitales féminines,https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_de_la_tol%C3%A9rance_z%C3%A9ro_%C3%A0_l%27%C3%A9gard_des_mutilations_g%C3%A9nitales_f%C3%A9minines
11 février,Journée internationale des femmes et des filles de science,https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_des_femmes_et_des_filles_de_science
8 mars,Journée internationale des droits des femmes,https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_des_femmes
14 avril,Cake and Cunnilingus Day,https://fr.wikipedia.org/wiki/Cake_and_Cunnilingus_Day
28 septembre,Journée internationale pour le droit à l'avortement,https://en.wikipedia.org/wiki/International_Safe_Abortion_Day
11 octobre,Journée internationale des droits des filles,https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_de_la_fille
25 novembre,Journée internationale pour l'élimination de la violence à l'égard des femmes,https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_pour_l%27%C3%A9limination_de_la_violence_%C3%A0_l%27%C3%A9gard_des_femmes
6 décembre,Journée nationale de commémoration et d'action contre la violence faite aux femmes,https://fr.wikipedia.org/wiki/Journ%C3%A9e_nationale_de_comm%C3%A9moration_et_d%27action_contre_la_violence_faite_aux_femmes
```

and with nsv:

```nsv
date
titre
lien

6 février
Journée internationale de la tolérance zéro à l'égard des mutilations génitales féminines
https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_de_la_tol%C3%A9rance_z%C3%A9ro_%C3%A0_l%27%C3%A9gard_des_mutilations_g%C3%A9nitales_f%C3%A9minines

11 février
Journée internationale des femmes et des filles de science
https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_des_femmes_et_des_filles_de_science

8 mars
Journée internationale des droits des femmes
https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_des_femmes

14 avril
Cake and Cunnilingus Day
https://fr.wikipedia.org/wiki/Cake_and_Cunnilingus_Day

28 septembre
Journée internationale pour le droit à l'avortement
https://en.wikipedia.org/wiki/International_Safe_Abortion_Day

11 octobre
Journée internationale des droits des filles
https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_de_la_fille

25 novembre
Journée internationale pour l'élimination de la violence à l'égard des femmes
https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_pour_l%27%C3%A9limination_de_la_violence_%C3%A0_l%27%C3%A9gard_des_femmes

6 décembre
Journée nationale de commémoration et d'action contre la violence faite aux femmes
https://fr.wikipedia.org/wiki/Journ%C3%A9e_nationale_de_comm%C3%A9moration_et_d%27action_contre_la_violence_faite_aux_femmes
```

Not that the csv is bad, but I find the nsv easier to read as a human with a terminal or a text editor. NSV is doing quite nice to pretty print tabular data or build parsable tabular data by hand for later used. If you need something more powerful it is common to use JSON or YAML, and shout-out to [TOML](https://github.com/toml-lang/toml) and [Dhall](https://github.com/dhall-lang/dhall-lang/).

### NSV and SRT


The following `.srt` file from _Star Wars: Episode II – Attack of the Clones_ is almost `.nsv` file. We have to wrap the multiline and we are good:

```srt
1
00:02:16,612 --> 00:02:19,376
"Senator, we're making
our final approach into Coruscant."

2
00:02:19,482 --> 00:02:21,609
Very good, Lieutenant.

3
00:03:13,336 --> 00:03:15,167
We made it.

4
00:03:18,608 --> 00:03:20,371
I guess I was wrong.

5
00:03:20,476 --> 00:03:22,671
There was no danger at all.
```

and would parse to

<!-- note: no empty header in markdown header -->
<table>
<tbody>
<tr>
<td>1</td>
<td>00:02:16,612 --&gt; 00:02:19,376</td>
<td>Senator, we&#39;re making<br/>our final approach into Coruscant.</td>
</tr>
<tr>
<td>2</td>
<td>00:02:19,482 --&gt; 00:02:21,609</td>
<td>Very good, Lieutenant.</td>
</tr>
<tr>
<td>3</td>
<td>00:03:13,336 --&gt; 00:03:15,167</td>
<td>We made it.</td>
</tr>
<tr>
<td>4</td>
<td>00:03:18,608 --&gt; 00:03:20,371</td>
<td>I guess I was wrong.</td>
</tr>
<tr>
<td>5</td>
<td>00:03:20,476 --&gt; 00:03:22,671</td>
<td>here was no danger at all.</td>
</tr>
</tbody>
</table>

and in `CSV`

```csv
1,"00:02:16,612 --> 00:02:19,376","Senator, we're making
our final approach into Coruscant."
2,"00:02:19,482 --> 00:02:21,609","Very good, Lieutenant."
3,"00:03:13,336 --> 00:03:15,167",We made it.
4,"00:03:18,608 --> 00:03:20,371",I guess I was wrong.
5,"00:03:20,476 --> 00:03:22,671",here was no danger at all.
```

## Basic rules

NSV specifications and implementations are as follows: 

* NSV is a delimited data format that has fields/columns separated by one newline character and records/rows terminated by two newlines.
* A NSV file does not require a specific character encoding, byte order, or line terminator format (some software may not support all line-end variations).
* A record ends at two empty lines. However, line terminators can be embedded as data within fields, so software must recognize quoted line-separators (see below) in order to correctly assemble an entire record from perhaps multiple lines.
* All records should have the same number of fields, in the same order.
* Data within fields is interpreted as a sequence of characters, not as a sequence of bits or bytes (see RFC 2046, section 4.1). For example, the numeric quantity 65535 may be represented as the 5 ASCII characters "65535" (or perhaps other forms such as "0xFFFF", "000065535.000E+00", etc.); but not as a sequence of 2 bytes intended to be treated as a single binary integer rather than as two characters (e.g. the numbers 11264–11519 have a comma as their high order byte: `ord(',')*256..ord(',')*256+255`). If this "plain text" convention is not followed, then the CSV file no longer contains sufficient information to interpret it correctly, the CSV file will not likely survive transmission across differing computer architectures, and will not conform to the `text/csv` MIME type.
* Any field _may_ be quoted (that is, enclosed within double-quote characters), while some fields must be quoted, as specified in the following rules and examples:

```
"1997"
"Ford"
"E350"
```

* Fields with embedded line breaks or double-quote characters must be quoted.

```
1997
Ford
E350
"Super
luxurious truck"
```

* Each of the embedded double-quote characters must be represented by a pair of double-quote characters.

```
1997
Ford
E350
"Super
""luxurious"" truck"
```

* The first record may be a "header", which contains column names in each of the fields (there is no reliable way to tell whether a file does this or not; however, it is uncommon to use characters other than letters, digits, and underscores in such column names).

```
Year
Make
Model

1997
Ford
E350

2000
Mercury
Cougar
```

## Name

NSV is the acronym of Newline-separated values. It is not correlated to an [authoritarian regime weapon](https://en.wikipedia.org/wiki/NSV_machine_gun) or a [nazi charity](https://en.wikipedia.org/wiki/National_Socialist_People%27s_Welfare). Don't mix them with this project but feel free to punch them ([Is it really ok to punch nazis?](https://www.youtube.com/watch?v=IKICKcMU3MU)). A previous [nsv file format](https://en.wikipedia.org/wiki/Nullsoft) made by Nullsoft, Inc. used to exist but this specifict file format was defunct in 2014.

Some alternative name could be BSV (Break-separated values) or LSV (Line-separated values). What do you think?

## Next steps

- Talk about the idea with people
- Explore the limit of the idea like how we want to escape stuff,e tc.
- Fix the naming
- Handle the empty string vs null case
- Write a RFC based on [rfc4180](https://datatracker.ietf.org/doc/html/rfc4180)
- Write a reference implementation to parse nsv and print to nsv
- Write a reference implementation to convert from and to csv
- Write an extension to handle NSV on vscode

## Authors and acknowledgment

Mostly adapted from Wikipedia article (mainly https://en.wikipedia.org/wiki/Comma-separated_values)

## License

AGPL3
